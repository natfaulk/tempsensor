#include <Wire.h>

#include "adc.hpp"
#include "debug.hpp"
#include "constants.hpp"

const uint8_t DEV_ADDR = 0x4D;
const double REF_V = 3.3;
const double ADC_MAX = 4095.0;

void ADC_Init(void)
{
  Wire.begin(21, 22);

  // need to initialise mosfet pin for v1.1 devices
#if NATFAULK_DEVICE == NATFAULK_V1_1
  pinMode(V_DIVIDER_NFET_PIN, OUTPUT);
  digitalWrite(V_DIVIDER_NFET_PIN, 0);
#endif
}

uint16_t ADC_GetRaw(void)
{
  uint8_t count = 0;
  uint16_t out = 0;
  Wire.requestFrom(DEV_ADDR, uint8_t(2)); // explicitely cast as uint8_t to avoid arduino warning
  while(Wire.available())
  { 
    // in case more bytes than expected
    if (count > 1)
    {
      DBG_s("Too many bytes in ADC read!");
      break;
    }
    out |= (Wire.read() << (8 * (1 - count)));
    count++;
  }
  Wire.endTransmission();
  return out;
}

double ADC_GetV(void)
{
  // returns half the Voltage due to the voltage divider to make sure is < 3.3v
  // need to first turn on MOSFET if on the V1.1 devices to connect the divider.
#if NATFAULK_DEVICE == NATFAULK_V1_1
  digitalWrite(V_DIVIDER_NFET_PIN, 1);
  delay(10);
#endif
  
  double adcVal = ADC_GetRaw();

#if NATFAULK_DEVICE == NATFAULK_V1_1
  digitalWrite(V_DIVIDER_NFET_PIN, 0);
#endif

  return REF_V * (adcVal / ADC_MAX) * 2;
}