const path = require('path')
const fs = require('fs')

const INPUT_FILE = 'index.html'
const OUTPUT_FILE = 'index.html.hpp'

;(()=>{
  if (!fs.existsSync(INPUT_FILE)) {
    console.log(`[W2CPP] Could not find input file: ${INPUT_FILE}`)
    process.exit(-1)
  }

  let data = fs.readFileSync(INPUT_FILE, 'utf8')
  let dataOut = '#ifndef INDEX_HTML_HPP\n#define INDEX_HTML_HPP\n\nconst char* WEBPAGE_HTML = "'

  for (let i = 0; i < data.length; i++) {
    let _char = data[i]
    if (_char === '\r') continue;
    if (_char === '"') dataOut += '\\'
    if (_char === '\n') dataOut += '\\n\\'
    dataOut += _char
  }

  dataOut += '";\n\n#endif\n'
  fs.writeFileSync(path.join(__dirname, '..', OUTPUT_FILE), dataOut)
})()

