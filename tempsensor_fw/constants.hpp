#ifndef CONSTANTS_HPP
#define CONSTANTS_HPP

#include <stdint.h>
#include <stddef.h>

#define NATFAULK_LOCAL_SERVER 0

#define NATFAULK_V1     1
#define NATFAULK_V1_1   2
#define NATFAULK_MOCK   3

#define NATFAULK_DEVICE NATFAULK_V1_1

static const char*     VERSION                                 = "1.3.0";

static const uint32_t  BAUD_RATE                               = 115200;
static const uint32_t  WIFI_CONNECT_TIMEOUT                    = 30 * 1000; // 30s
static const uint8_t   MAX_SEND_ATTEMPTS                       = 5;
static const uint32_t  DEEP_SLEEP_TIME                         = 10 * 60 * 1000 * 1000; // in microseconds

#if NATFAULK_LOCAL_SERVER
static const char*     HOST_ADDR                               = "192.168.1.162";
static const uint32_t  HOST_PORT                               = 3001;
static const char*     HOST_URL                                = "/add_data";
#else
static const char*     HOST_ADDR                               = "temperature.natfaulk.com";
static const uint32_t  HOST_PORT                               = 80;
static const char*     HOST_URL                                = "/add_data";
#endif

static const size_t    API_KEY_SIZE_BYTES                      = 128 / 8; // 128bit key

// Is the same between all the different hardwares thus far
static const uint32_t  USER_BUTTON_PIN                         = 32;

#if NATFAULK_DEVICE == NATFAULK_V1_1
static const uint32_t  V_DIVIDER_NFET_PIN                      = 5;
#endif

#endif
