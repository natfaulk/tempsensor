#include "md5.hpp"

#include <rom/md5_hash.h>
#include <string.h>

struct MD5Context myContext;
uint8_t data[16];

uint8_t* md5(const char* buf)
{
  return md5((const uint8_t*)buf, strlen(buf));
}

uint8_t* md5(const uint8_t* buf, uint32_t len)
{
  memset(&myContext, 0x00, sizeof(myContext));
  memset(data, 0x00, 16);
  MD5Init(&myContext);
  MD5Update(&myContext, buf, len);
  MD5Final(data, &myContext);
  return data;
}
