#ifndef DEVICE_HPP
#define DEVICE_HPP

#include <Arduino.h>

const char* DEV_GetID(void);
const char* DEV_GetMacAddr(void);
const char* DEV_GetApiKey(void);
bool DEV_SetApiKey(const char* _apikey);
bool DEV_ApiKeyExist(void);
bool DEV_JoinWiFi(void);
bool DEV_TryConnectWifi(const char* _ssid, const char* _pwd);
bool DEV_UpdateWifiCreds(const char* _ssid, const char* _pwd);
bool DEV_ApiKeyHexToCharArray(const char* hexString, char* out);

#endif
