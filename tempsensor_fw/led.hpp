#ifndef LED_HPP
#define LED_HPP

#include <stdint.h>

class Led {
public:
  Led(uint8_t _pin);
  void on(void);
  void off(void);
  void blockingFlash(uint32_t count, uint32_t onTime, uint32_t offTime);
private:
  uint8_t pin;
};

#endif
