#include "rgbLed.hpp"
#include "espTimer.hpp"

SingleColour::SingleColour(uint8_t _pin): pin(_pin), val(0)
{
  pinMode(pin, OUTPUT);
  digitalWrite(pin, 0);
  timer = GetNextTimer();
  ledcSetup(timer, 5000, 8);
  ledcAttachPin(pin, timer);
  this->setVal(0);
}

void SingleColour::setVal(uint8_t _val)
{
  val = _val;
  ledcWrite(timer, val);
}

RgbLed::RgbLed(uint8_t _pinR, uint8_t _pinG, uint8_t _pinB)
{
  r = new SingleColour(_pinR);
  g = new SingleColour(_pinG);
  b = new SingleColour(_pinB);
}

void RgbLed::setRGB(uint8_t _r, uint8_t _g, uint8_t _b)
{
  r->setVal(_r);
  g->setVal(_g);
  b->setVal(_b);
}

void RgbLed::on(void)
{

}

void RgbLed::off(void)
{
  r->setVal(0);
  g->setVal(0);
  b->setVal(0);
}