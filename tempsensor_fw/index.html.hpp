#ifndef INDEX_HTML_HPP
#define INDEX_HTML_HPP

const char* WEBPAGE_HTML = "<!doctype html>\n\
\n\
<html lang=\"en\">\n\
<head>\n\
  <meta charset=\"UTF-8\" name=\"viewport\" content=\"width=device-width, initial-scale=1\">\n\
\n\
  <title>NATFAULK Temperature sensor config</title>\n\
\n\
  <!-- <link rel=\"stylesheet\" href=\"css/styles.css?v=1.0\"> -->\n\
  <style>\n\
    .grid-container{\n\
      max-width: 500px;\n\
      display: grid;\n\
      grid-template-columns: auto auto;\n\
    }\n\
  </style>\n\
</head>\n\
\n\
<body>\n\
\n\
  <h1>NATFAULK Temperature sensor config page</h1>\n\
\n\
  <h2>Wi-Fi Details</h2>\n\
\n\
  <div class=\"grid-container\">\n\
    <div>\n\
      SSID:\n\
    </div>\n\
    <div>\n\
      <input type=\"text\" id=\"ssid\" placeholder=\"ssid\">\n\
    </div>\n\
  \n\
    <div>\n\
      Password:\n\
    </div>\n\
    <div>\n\
      <input type=\"text\" id=\"pwd\" placeholder=\"pwd\">\n\
    </div>\n\
    \n\
    <div>\n\
      <button id=\"submit\">Submit</button>\n\
    </div>\n\
  </div>\n\
\n\
  <h2>API Key Details</h2>\n\
  <div>\n\
    DEVICE ID: <span id=\"device-id\">Loading...</span>\n\
  </div>\n\
  <div>\n\
    CURRENT API KEY: <span id=\"current-api-key\">Loading...</span>\n\
  </div>\n\
  <div class=\"grid-container\">\n\
    <div>\n\
      New API key:\n\
    </div>\n\
    <div>\n\
      <input type=\"text\" id=\"api\" placeholder=\"new API key\">\n\
      <button id=\"randomise\">Random Key</button>\n\
    </div>\n\
\n\
    <div>\n\
      <button id=\"submit2\">Submit</button>\n\
    </div>\n\
  </div>\n\
\n\
  <script>\n\
    document.getElementById('submit').addEventListener('click', () => {\n\
      console.log('this ran');\n\
      let ssid = document.getElementById('ssid').value;\n\
      let pwd = document.getElementById('pwd').value;\n\
\n\
      let formData = new FormData();\n\
      formData.append('ssid', ssid);\n\
      formData.append('pwd', pwd);\n\
\n\
      fetch('/updateWifi', {\n\
        method: 'POST',\n\
        body: formData\n\
      })\n\
      .then(response => response.text())\n\
      .then(data => {\n\
        console.log('Response:', data);\n\
      })\n\
      .catch((error) => {\n\
        console.error('Error:', error);\n\
      });\n\
    });\n\
\n\
    document.getElementById('submit2').addEventListener('click', () => {\n\
      let newapikey = document.getElementById('api').value;\n\
      let formData = new FormData();\n\
      formData.append('newapikey', newapikey);\n\
      fetch('/updateApiKey', {\n\
        method: 'POST',\n\
        body: formData\n\
      })\n\
      .then(response => response.text())\n\
      .then(data => {\n\
        console.log('Response:', data);\n\
      })\n\
      .catch((error) => {\n\
        console.error('Error:', error);\n\
      });\n\
    });\n\
\n\
    document.getElementById('randomise').addEventListener('click', () => {\n\
      const NUM_DIGITS = (128 / 8) * 2;/* 128 bit, 16 byte, 32 hex chars */\n\
      /* not cryptographically secure but shouldn't be a problem for now */\n\
      /* also not necessarily the best way to do it... */\n\
      const characters = '0123456789abcdef';\n\
      let out = '';\n\
      for (let i = 0; i < NUM_DIGITS; i++) {\n\
        out += characters[Math.floor(Math.random() * 16)];\n\
      }\n\
\n\
      document.getElementById('api').value = out;\n\
    })\n\
\n\
    fetch('/currentApiKey').then(response => response.text())\n\
    .then(data => {\n\
      document.getElementById('current-api-key').innerHTML = data;\n\
    })\n\
    .catch((error) => {\n\
      console.error('Error:', error);\n\
    });\n\
\n\
    fetch('/deviceID').then(response => response.text())\n\
    .then(data => {\n\
      document.getElementById('device-id').innerHTML = data\n\
    })\n\
    .catch((error) => {\n\
      console.error('Error:', error);\n\
    });\n\
  </script>\n\
</body>\n\
</html>\n\
";

#endif
