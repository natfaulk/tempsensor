#include "portal.hpp"
#include "debug.hpp"
#include "constants.hpp"
#include "device.hpp"
#include "index.html.hpp"
#include "device.hpp"

#include <Arduino.h>
#include <WiFi.h>
#include <WebServer.h>

void handleIndex(void);
void handleNotFound(void);
void handleUpdateWifi(void);
void handleUpdateApiKey(void);
void handleCurrentApiKey(void);
void handleDeviceID(void);
const char* makeAPSSID(void);

WebServer* server = nullptr;
void PTL_StartAPServer(void)
{
  const char* ssid = makeAPSSID();
  DBG_pair("[PTL] Starting access point with SSID", ssid);
  WiFi.softAP(ssid, "YEETYEET");

  IPAddress IP = WiFi.softAPIP();
  DBG_pair("[PTL] AP IP address", IP.toString());

  server = new WebServer(80);
  server->on("/", handleIndex);
  server->on("/updateWifi", handleUpdateWifi);
  server->on("/updateApiKey", handleUpdateApiKey);
  server->on("/currentApiKey", handleCurrentApiKey);
  server->on("/deviceID", handleDeviceID);
  server->onNotFound(handleNotFound);
  server->begin();

  while (1)
  {
    yield();

    server->handleClient();
  }
}

void handleIndex(void)
{
  DBG_s("[PTL] Sent index page");
  server->send(200, "text/html", WEBPAGE_HTML); 
}

void handleNotFound(void)
{
  DBG_s("[PTL] Sent 404");
  server->send(404, "text/plain", "404: page could not be found");
}

void handleUpdateWifi(void)
{
  if (!server->hasArg("ssid")) {
    DBG_s("[PTL] Sent 500, missing ssid");
    server->send(500, "text/plain", "missing ssid");
    return;
  }

  if (!server->hasArg("pwd")) {
    DBG_s("[PTL] Sent 500, missing pwd");
    server->send(500, "text/plain", "missing pwd");
    return;
  }

  String ssid = server->arg("ssid");
  String pwd = server->arg("pwd");

  if (DEV_UpdateWifiCreds(ssid.c_str(), pwd.c_str()))
  {
    DBG_s("[PTL] Sent 200, successfully updated credentials, rebooting...");
    server->send(200, "text/html", "success");
    ESP.restart();
  }

  DBG_s("[PTL] Sent 500, couldn't save credentials");
  server->send(500, "text/plain", "couldn't save credentials");
}

void handleUpdateApiKey(void)
{
  if (!server->hasArg("newapikey")) {
    DBG_s("[PTL] Sent 500, missing api key");
    server->send(500, "text/plain", "missing api key");
    return;
  }

  String newApiKey = server->arg("newapikey");
  char apiKeyConverted[API_KEY_SIZE_BYTES + 1];

  // convert key from hex string to char array
  if (!DEV_ApiKeyHexToCharArray(newApiKey.c_str(), apiKeyConverted))
  {
    DBG_s("[PTL] Sent 500, failed to convert api key to char array");
    server->send(500, "text/plain", "failed to convert api key to char array");
    return;
  }

  if (DEV_SetApiKey(apiKeyConverted))
  {
    DBG_s("[PTL] Sent 200, successfully updated API key, rebooting...");
    server->send(200, "text/html", "success");
    ESP.restart();
    return;
  }

  DBG_s("[PTL] Sent 500, couldn't update API key");
  server->send(500, "text/plain", "couldn't update API key");
}

void handleCurrentApiKey(void)
{
  if (!DEV_ApiKeyExist()) {
    DBG_s("[PTL] Handle /currentApiKey, No API key exists...");
    server->send(200, "text/html", "None exists");
    return;
  }

  const char* apiKey = DEV_GetApiKey();
  if (strlen(apiKey) == 0) {
    DBG_s("[PTL] Handle /currentApiKey, Failed to read API key...");
    server->send(200, "text/html", "Failed to read API key");
    return;
  }

  DBG_pair("[PTL] Handle /currentApiKey, Current API key", apiKey);
  server->send(200, "text/html", apiKey);
}

void handleDeviceID(void)
{
  const char* deviceID = DEV_GetID();
  DBG_pair("[PTL] Handle /deviceID, Device ID", deviceID);
  server->send(200, "text/html", deviceID);
}

char apSsid[32+1];
const char* makeAPSSID(void)
{
  const char* id = DEV_GetID();
  const char* last4id = id + (strlen(id) - 4);
  sprintf(apSsid, "NF_temp_%s", last4id);
  return apSsid;
}

