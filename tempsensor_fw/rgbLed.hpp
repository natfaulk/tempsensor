#ifndef RGBLED_HPP
#define RGBLED_HPP

#include <stdint.h>
#include <Arduino.h>

class SingleColour {
public:
  SingleColour(uint8_t _pin);
  void setVal(uint8_t _val);
private:
  uint8_t pin;
  uint8_t val;
  uint8_t timer;
};

class RgbLed {
public:
  RgbLed(uint8_t _pinR, uint8_t _pinG, uint8_t _pinB);
  void setRGB(uint8_t _r, uint8_t _g, uint8_t _b);
  void on(void);
  void off(void);
private:
  SingleColour* r;
  SingleColour* g;
  SingleColour* b;
};

#endif