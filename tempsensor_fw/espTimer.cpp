#include "espTimer.hpp"
#include "debug.hpp"

static uint8_t currentTimer = 0;

uint8_t GetNextTimer(void)
{
  currentTimer++;
  if (currentTimer >= 8) DBG_s("Too many timers used");
  return currentTimer - 1;
}