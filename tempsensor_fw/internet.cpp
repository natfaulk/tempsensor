#include "internet.hpp"
#include "debug.hpp"
#include "constants.hpp"
#include "device.hpp"

#include <HTTPClient.h>
#include <WiFi.h>

bool sendData(float temp, float batV)
{
  if (WiFi.status() != WL_CONNECTED)
  {
    DBG_s("WiFi not connected. Cannot send data.");
    return false;
  }

  String postData = "{\"temp\":";
  postData += temp;
  postData += ",\"batV\":";
  postData += batV;
  postData += ",\"version\":\"";
  postData += VERSION;
  postData += "\",\"ID\":\"";
  postData += DEV_GetID();
  postData += "\",\"apiKey\":\"";
  if (DEV_ApiKeyExist()) postData += DEV_GetApiKey();
  else postData += "KEY_MISSING";
  postData += "\"}";
  DBG_pair("POST data", postData);

  HTTPClient http;
  http.begin(HOST_ADDR, HOST_PORT, HOST_URL);
  http.addHeader("Content-Type", "application/json");
  int httpResponseCode = http.POST(postData);

  bool out = true;

  // httpCode will be negative on error
  if (httpResponseCode > 0)
  {
    // HTTP header has been send and Server response header has been handled
    DBG_pair("HTTP response code", httpResponseCode);

    // file found at server
    if (httpResponseCode == HTTP_CODE_OK) {
      String payload = http.getString();
      
      if (payload != "{\"success\":true}")
      {
        DBG_s("Did not receive ack");
        out = false;
      }
    }
    else out = false;
  } else {
    DBG_pair("HTTP response code", httpResponseCode);
    DBG_pair("HTTP error", http.errorToString(httpResponseCode).c_str());
    out = false;
  }

  http.end();
  return out;
}
