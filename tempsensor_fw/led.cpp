#include "led.hpp"

#include <Arduino.h>

Led::Led(uint8_t _pin)
{
  this->pin = _pin;

  pinMode(this->pin, OUTPUT);
  this->off();
}

void Led::on(void)
{
  // low side control so inverted
  digitalWrite(this->pin, 0);
}

void Led::off(void)
{
  // low side control so inverted
  digitalWrite(this->pin, 1);
}

void Led::blockingFlash(uint32_t count, uint32_t onTime, uint32_t offTime)
{
  for (uint32_t i = 0; i < count; i++)
  {
    this->on();
    delay(onTime);
    this->off();
    delay(offTime);
  }
}

