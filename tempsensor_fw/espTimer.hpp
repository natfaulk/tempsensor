#ifndef ESP_TIMER_HPP
#define ESP_TIMER_HPP

#include <stdint.h>

uint8_t GetNextTimer(void);

#endif