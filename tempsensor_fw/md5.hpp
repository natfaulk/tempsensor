#ifndef MD5_HPP
#define MD5_HPP

#include <stdint.h>

uint8_t* md5(const char* buf);
uint8_t* md5(const uint8_t* buf, uint32_t len);

#endif
