#include "button.hpp"
#include "constants.hpp"

#include <Arduino.h>

void BUT_Init(void)
{
  pinMode(USER_BUTTON_PIN, INPUT);
}

bool BUT_GetUser(void)
{
  // Active low
  return !digitalRead(USER_BUTTON_PIN);
}

