#ifndef ADC_HPP
#define ADC_HPP

#include <stdint.h>
#include <Arduino.h>

void ADC_Init(void);
uint16_t ADC_GetRaw(void);
double ADC_GetV(void);

#endif