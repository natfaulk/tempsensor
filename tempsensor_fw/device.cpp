#include "device.hpp"
#include "md5.hpp"
#include "constants.hpp"
#include "debug.hpp"

#include <WiFi.h>
#include <esp_wifi.h>
#include <Preferences.h>

void apiKeyReadFromFlash(void);

// API key status
enum class ApiKeyStatus {
  NOT_INIT,
  NOT_FOUND,
  OK  
};

static Preferences prefs;

bool macAddrInit = false;
bool devIdInit = false;
ApiKeyStatus apiKeyInit = ApiKeyStatus::NOT_INIT;

char macAddr[12+1] = {0};
char devId[2*16 + 1] = {0};
char apiKey[2*API_KEY_SIZE_BYTES+1] = {0};

// Gets device ID. *WARNING* this is cached so once called will always return the same
// If one expects the device ID to change for some reason this function needs changing
const char* DEV_GetID(void)
{
  if (devIdInit) return devId;

  uint8_t* hash = md5(DEV_GetMacAddr());
  for (uint8_t i = 0; i < 16; i++) {
    sprintf(devId + 2*i, "%02X", hash[i]);
  }

  devIdInit = true;
  return devId;
}

// Gets mac address. *WARNING* this is cached so once called will always return the same
// If one expects the mac address to change for some reason this function needs changing
const char* DEV_GetMacAddr(void)
{
  if (macAddrInit) return macAddr;

  uint8_t mac[6];
  if(WiFiGenericClass::getMode() == WIFI_MODE_NULL){
    esp_read_mac(mac, ESP_MAC_WIFI_STA);
  }
  else{
    esp_wifi_get_mac(WIFI_IF_STA, mac);
  }

  sprintf(macAddr, "%02X%02X%02X%02X%02X%02X", mac[0], mac[1], mac[2], mac[3], mac[4], mac[5]);

  macAddrInit = true;
  return macAddr;
}

bool DEV_ApiKeyExist(void)
{
  if (apiKeyInit == ApiKeyStatus::NOT_INIT) apiKeyReadFromFlash();
  return (apiKeyInit == ApiKeyStatus::OK);
}

const char* DEV_GetApiKey(void)
{
  if (apiKeyInit == ApiKeyStatus::NOT_INIT) apiKeyReadFromFlash();
  if (apiKeyInit != ApiKeyStatus::OK) return "";
  return apiKey;
}

bool DEV_SetApiKey(const char* _apikey)
{
  if (apiKeyInit == ApiKeyStatus::NOT_INIT) apiKeyReadFromFlash();
  
  if (strlen(_apikey) > API_KEY_SIZE_BYTES) 
  {
    DBG_s("[DEV] API key too long");
    return false;
  }

  uint8_t apiBytes[API_KEY_SIZE_BYTES] = {0};
  memcpy(apiBytes, _apikey, strlen(_apikey));

  prefs.begin("natfaulk", false);
  size_t bytesWritten = prefs.putBytes("apiKey", (void *)apiBytes, API_KEY_SIZE_BYTES);
  prefs.end();

  if (bytesWritten != API_KEY_SIZE_BYTES)
  {
    DBG_s("[DEV] Incorrect number of bytes written to flash for API key");
    DBG_pair("[DEV] Bytes written", bytesWritten);
    return false;
  }

  apiKeyReadFromFlash();
  return apiKeyInit == ApiKeyStatus::OK;
}

bool DEV_ApiKeyHexToCharArray(const char* hexString, char* out)
{
  DBG_pair("[DEV] hexstring", hexString);

  uint32_t hexLen = strlen(hexString);
  if (hexLen > 2 * API_KEY_SIZE_BYTES) return false;
  
  uint32_t t;
  for (uint32_t i = 0; i < hexLen / 2; ++i)
  {
    // if conversion fails return false
    if (sscanf(&(hexString[2*i]), "%02x", &t) != 1) return false;
    // uint32_t down to char, should never be more than 2 hex digits so all good
    out[i] = (char)t;
  }
  out[hexLen / 2] = '\0';

  return true;
}

bool DEV_JoinWiFi(void)
{
  prefs.begin("natfaulk", true);
  String ssid = prefs.getString("ssid");
  String pwd = prefs.getString("pwd");
  prefs.end();

  if (
    ssid.length()
    && pwd.length()
    && DEV_TryConnectWifi(ssid.c_str(), pwd.c_str())
    ) return true;
  
  return false;
}

bool DEV_TryConnectWifi(const char* _ssid, const char* _pwd)
{
  DBG_s("[DEV] Connecting to WIFI network ", false);
  DBG_s(_ssid);

  WiFi.begin(_ssid, _pwd);

  DBG_s("[DEV] ", false);
  uint32_t startTime = millis();
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    DBG_s(".", false);

    if (millis() - startTime > WIFI_CONNECT_TIMEOUT) break;
  }
  DBG_nl();

  if (WiFi.status() == WL_CONNECTED)
  {
    DBG_s("[DEV] WiFi connection successful");
    DBG_pair("[DEV] IP address", WiFi.localIP());
    return true;
  }

  DBG_s("[DEV] Connection to WIFI timed out...");
  return false;
}

bool DEV_UpdateWifiCreds(const char* _ssid, const char* _pwd)
{
  if (strlen(_ssid) == 0 || strlen(_pwd) == 0) return false;

  prefs.begin("natfaulk", false);
  prefs.putString("ssid", _ssid);
  prefs.putString("pwd", _pwd);
  prefs.end();

  return true;
}

void apiKeyReadFromFlash(void)
{
  uint8_t apiBytes[API_KEY_SIZE_BYTES] = {0};
  prefs.begin("natfaulk", false);
  size_t bytesRead = prefs.getBytes("apiKey", (void *)apiBytes, API_KEY_SIZE_BYTES);
  prefs.end();

  if (bytesRead == 0)
  {
    DBG_s("[DEV] API key not found in flash");
    apiKeyInit = ApiKeyStatus::NOT_FOUND;
    return;
  }

  if (bytesRead != API_KEY_SIZE_BYTES) 
  {
    DBG_s("[DEV] Incorrect number of bytes retrieved from flash for API key");
    DBG_pair("[DEV] Bytes retrieved", bytesRead);
    apiKeyInit = ApiKeyStatus::NOT_FOUND;
    return;
  }
  // convert to hex representation
  for (uint8_t i = 0; i < API_KEY_SIZE_BYTES; i++) {
    sprintf(apiKey + 2*i, "%02X", apiBytes[i]);
  }

  apiKeyInit = ApiKeyStatus::OK;
}

