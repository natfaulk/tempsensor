#include "debug.hpp"
#include "adc.hpp"
#include "constants.hpp"
#include "rgbLed.hpp"
#include "internet.hpp"
#include "device.hpp"
#include "portal.hpp"
#include "led.hpp"
#include "button.hpp"

#include <OneWire.h>
#include <DallasTemperature.h>
#include <Wire.h>
#include <WiFi.h>
#include <memory>

#include "lwip/dns.h"

void printWakeupReason(void);

#if NATFAULK_DEVICE == NATFAULK_V1
const uint8_t DS18B20_PIN = 4;
const uint8_t DS18B20_GND_PIN = 5;
std::shared_ptr<RgbLed> led;
#elif NATFAULK_DEVICE == NATFAULK_V1_1
const uint8_t DS18B20_PIN = 17;
const uint8_t LED1_PIN = 4;
const uint8_t LED2_PIN = 16;
std::shared_ptr<Led> led1;
std::shared_ptr<Led> led2;
#else
const uint8_t DS18B20_PIN = 4;
#endif

OneWire ds(DS18B20_PIN);
DallasTemperature tempSensor(&ds);


RTC_DATA_ATTR bool firstwake = true;
uint8_t sendAttempts = 0;

void setup(void)
{
  DBG_Setup(BAUD_RATE);
  DBG_pair("FW version", VERSION);
  DBG_s("Setup started");
  printWakeupReason();

#if NATFAULK_DEVICE == NATFAULK_MOCK
  DBG_s("FW running on mock device");
#elif NATFAULK_DEVICE == NATFAULK_V1
  // use GPIO as GND pin
  DBG_s("Initialising temp sensor...");
  pinMode(DS18B20_GND_PIN, OUTPUT);
  digitalWrite(DS18B20_GND_PIN, 0);
  delay(10);

  tempSensor.begin();

  DBG_s("Initialising ADC...");
  ADC_Init();

  led = std::make_shared<RgbLed>(16, 17, 18);
  DBG_s("Initialising RGB LED");

  if (firstwake)
  {
    firstwake = false;
    DBG_s("Led test started...");

    led->setRGB(1, 0, 0);
    delay(1000);
    led->setRGB(0, 1, 0);
    delay(1000);  
    led->setRGB(0, 0, 1);
    delay(1000);  
    led->setRGB(0, 0, 0);
    DBG_s("Led test finished");
  }
#elif NATFAULK_DEVICE == NATFAULK_V1_1
  DBG_s("FW running on V1.1 device");
  DBG_s("Initialising temp sensor...");

  led1 = std::make_shared<Led>(LED1_PIN);
  led2 = std::make_shared<Led>(LED2_PIN);

  led1->blockingFlash(3, 100, 100);
  led2->blockingFlash(3, 50, 150);

  // give sensor time to start
  delay(50);
  tempSensor.begin();

  DBG_s("Initialising ADC...");
  ADC_Init();
#else
  DBG_s("FW running on unknown device!");
#endif

  BUT_Init();
  if (BUT_GetUser()) 
  {
    DBG_s("User button pressed, starting AP server...");
    PTL_StartAPServer();
  }

  // sleep if can't join wifi
  if (!DEV_JoinWiFi()) deepSleep();

  // using cloudflare DNS as was having problems with dns lookups failing
  ip_addr_t dnsserver;
  IP_ADDR4(&dnsserver, 1,1,1,1);
  dns_setserver(0, &dnsserver);

  // if (DEV_SetApiKey("TEST_API_KEY")) DBG_s("API key set successfully");
  // else DBG_s("Failed to set API key");

  DBG_pair("Device MAC address", DEV_GetMacAddr());
  DBG_pair("Device ID", DEV_GetID());
  if (DEV_ApiKeyExist()) DBG_pair("API Key", DEV_GetApiKey());
  else DBG_s("Please add an API key!");
  
  DBG_s("Setup finished");
}

void loop(void)
{
#if NATFAULK_DEVICE == NATFAULK_MOCK
  float temp = 20 + double(random(500))/100.0;
  double batV = 3.5 + double(random(100))/100.0;
#else
  tempSensor.requestTemperatures();
  float temp = tempSensor.getTempCByIndex(0);
  double batV = ADC_GetV();
#endif

  DBG_pair("Temp", temp);
  DBG_pair("ADC", batV);

#if NATFAULK_DEVICE == NATFAULK_V1_1
  led1->on();
#endif

  bool datasent = sendData(temp, batV);
#if NATFAULK_DEVICE == NATFAULK_V1_1
  led1->off();
#endif
  
  if (datasent) DBG_s("Data sent successfully");
  else {
    DBG_s("Data failed to send");
#if NATFAULK_DEVICE == NATFAULK_V1_1
    led2->blockingFlash(5, 100, 100);
#endif
  }

  sendAttempts++;

  if (sendAttempts >= MAX_SEND_ATTEMPTS || datasent)
  {
    deepSleep();
  }

  delay(1000);
}

void deepSleep(void)
{
  esp_sleep_enable_timer_wakeup(DEEP_SLEEP_TIME);
  DBG_s("Sleeping now...");
  esp_deep_sleep_start();
}

void printWakeupReason(void)
{
  esp_sleep_wakeup_cause_t wakeup_reason;
  wakeup_reason = esp_sleep_get_wakeup_cause();

  switch(wakeup_reason)
  {
    case ESP_SLEEP_WAKEUP_EXT0 : DBG_s("Wakeup caused by external signal using RTC_IO"); break;
    case ESP_SLEEP_WAKEUP_EXT1 : DBG_s("Wakeup caused by external signal using RTC_CNTL"); break;
    case ESP_SLEEP_WAKEUP_TIMER : DBG_s("Wakeup caused by timer"); break;
    case ESP_SLEEP_WAKEUP_TOUCHPAD : DBG_s("Wakeup caused by touchpad"); break;
    case ESP_SLEEP_WAKEUP_ULP : DBG_s("Wakeup caused by ULP program"); break;
    default : DBG_pair("Wakeup was not caused by deep sleep", wakeup_reason); break;
  }
}
