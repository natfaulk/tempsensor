import React from 'react'
import DeviceChooser from './DevicesChooser'
import DayChooser from './DayChooser'
import TemperatureGraph from './TemperatureGraph'

import * as ApiCalls from './apiCalls'

class Devices extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      devices: {},
      devicesList: [],
      currentDevice: '',
      files: [],
      currentFile: ''
    }

    this.handleDeviceChange = this.handleDeviceChange.bind(this)
    this.handleFileChange = this.handleFileChange.bind(this)
  }

  async componentDidMount() {
    let devicesList = await ApiCalls.getAllDevices()
    let currentDevice = (devicesList.length > 0)?devicesList[0].ID:''

    let devices = {}
    devicesList.forEach(_device => {
      devices[_device.ID] = _device
    })

    this.setState({
      devicesList,
      devices,
      currentDevice
    })
    this.handleDeviceChange(currentDevice)

    console.log(devices, currentDevice)
  }

  async handleDeviceChange(device) {
    console.log('this ran')
    this.setState({currentDevice: device})
    
    let devFiles = await ApiCalls.getDeviceFiles(device)
    console.log(devFiles)
    this.setState({
      files: devFiles,
      currentFile: (devFiles.length > 0)?devFiles[0]:''
    })
  }

  handleFileChange(newfile) {
    this.setState({currentFile: newfile})
  }

  getDeviceBatV() {
    let currentDevice = this.state.devices[this.state.currentDevice]
    if (currentDevice === undefined) return ''
    else return currentDevice.batV
  }

  render() {
    return (
      <>
        <div className="centre">
          <span>Choose device: </span>
          <DeviceChooser devices={this.state.devicesList} currentDevice={this.state.currentDevice} onDeviceChange={this.handleDeviceChange} />
          <br />
          <span>Choose date: </span>
          <DayChooser days={this.state.files} currentDay={this.state.currentFile} onDayChange={this.handleFileChange} />
        </div>
        <div>
          <TemperatureGraph currentDevice={this.state.currentDevice} currentFile={this.state.currentFile} />
        </div>
        <div>
          Battery voltage: {this.getDeviceBatV()}<br />
        </div>
      </>
    )
  }
}

export default Devices
