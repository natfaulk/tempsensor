import React from 'react';

class DayChooser extends React.Component {
  constructor(props) {
    super(props)

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    this.props.onDayChange(event.target.value)
  }

  render() {
    return (
      <select value={this.props.currentDay} onChange={this.handleChange}>
        <option disabled value="">Select a day</option>
        {this.props.days.map(_day => <option key={_day} value={_day}>{_day}</option>)}
      </select>
    )
  }
}

export default DayChooser;
