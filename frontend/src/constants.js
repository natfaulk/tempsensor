export const API_ALL_DEVICES      = '/alldevices'
export const API_DEVICE_FILES     = '/datafilenames'
export const API_SINGLE_FILE      = '/datafiles'
export const API_DEVICE_INFO      = '/deviceinfo'
