import React from 'react';
import './App.css';
import Devices from './Devices'

class App extends React.Component {
  render() {
    return <>
      <div id="outer">
        <div>
          <h1>My room temperature</h1> 
          <p>Because why not...</p>
          <Devices />
        </div>
      </div>
    </>
  }
}

export default App;
