import * as Constants from './constants'

export const getAllDevices = async () => {
  let out = []
  await fetch(Constants.API_ALL_DEVICES)
  .then(response => response.json())
  .then(data => {
    data.sort((a, b) => (a.lastSeen < b.lastSeen) ? 1 : -1)
    // console.log(data)
    out = data
  }).catch((error) => {
    console.log(error)
    out = []
  })

  return out
}

export const getDeviceFiles = async _devId => {
  let out = []
  await fetch(Constants.API_DEVICE_FILES + `/${_devId}`)
  .then(response => response.json())
  .then(data => {
    data.sort((a, b) => (a < b) ? 1 : -1)
    // console.log(data)
    out = data
  }).catch((error) => {
    console.log(error)
    out = []
  })

  return out
}

export const getDeviceInfo = async _devId => {
  let out = []
  await fetch(Constants.API_DEVICE_INFO + `/${_devId}`)
  .then(response => response.json())
  .then(data => {
    console.log(data)
    out = data
  }).catch((error) => {
    console.log(error)
    out = {}
  })

  return out
}

export const getFile = async (_device, _file) => {
  let out = []

  await fetch(Constants.API_SINGLE_FILE + `/${_device}/${_file}`)
  .then(response => response.text())
  .then(data => {
    out = data
  }).catch((error) => {
    console.log(error)
  })

  return out
}


