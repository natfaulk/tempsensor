import React from 'react';
import Chart from 'react-google-charts'
import moment from 'moment'
import * as ApiCalls from './apiCalls'

const chartOptions = {
  title: 'Temperature in my room (Celsius)',
  curveType: 'function',
  legend: { position: 'bottom' }
}

export default class TemperatureGraph extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      chartData: [
        [
          { type: 'date', label: 'Time' },
          { type: 'number', label: 'Temperature' }
        ]
      ]
    }
  }

  async componentDidUpdate(prevProps) {
    if (
      (prevProps.currentFile !== this.props.currentFile && this.props.currentFile!=='')
      || prevProps.currentDevice !== this.props.currentDevice
      ) {
      let data = await ApiCalls.getFile(this.props.currentDevice, this.props.currentFile)
      
      let newChartData = [[
        { type: 'date', label: 'Time' },
        { type: 'number', label: 'Temperature' }
      ]]
  
      let loglist = data.split('\n')
      loglist.forEach(_ll => {
        // extra newline at the end causes a zero length string to try be decoded
        // adds extra noise to the console debugging
        if (_ll.length > 0) {
          try {
            let lljson = JSON.parse(_ll)
            newChartData.push([moment(lljson.time).toDate(), lljson.temp])
          } catch(e) {
            console.log('invalid json in log file', _ll)
          }
        }
      })
      
      this.setState({
        chartData: newChartData
      })
    }
  }

  render() {
    return (
      <Chart
        chartType="LineChart"
        loader={<div>Loading Chart</div>}
        data={this.state.chartData}
        options={chartOptions}
      />
    )
  }
}
