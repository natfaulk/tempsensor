import React from 'react';

class DeviceChooser extends React.Component {
  constructor(props) {
    super(props)

    this.handleChange = this.handleChange.bind(this)
  }

  handleChange(event) {
    this.props.onDeviceChange(event.target.value)
  }

  render() {
    return (
      <select value={this.props.currentDevice} onChange={this.handleChange}>
        <option disabled value="">Select a room</option>
        {this.props.devices.map(_device => <option key={_device.ID} value={_device.ID}>{_device.name} (ID: {_device.ID})</option>)}
      </select>
    )
  }
}

export default DeviceChooser;
