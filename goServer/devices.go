package main

import (
	"encoding/json"
	"fmt"
	"log"
	"os"
	"path/filepath"
)

type allDevices []struct {
	DevID      string  `json:"ID"`
	Name       string  `json:"name"`
	LastSeen   int64   `json:"lastSeen"`
	BatVoltage float32 `json:"batV"`
}

type deviceAPIKeysJSON map[string]string

const devicesFilename = "devices.json"
const apiKeysFilename = "apiKeys.json"

var devices allDevices
var apiKeys deviceAPIKeysJSON

func loadDevices() bool {
	f, err := os.Open(filepath.Join(saveDir, devicesFilename))
	if os.IsNotExist(err) {
		fmt.Println("Devices file does not exist. Creating...")
		return saveDevices()
	} else if err != nil {
		fmt.Println("Error opening devices file", err)
		return false
	}
	defer f.Close()

	decoder := json.NewDecoder(f)
	err = decoder.Decode(&devices)
	if err != nil {
		fmt.Println("Error parsing devices json file", err)
		return false
	}

	fmt.Println("Successfully loaded devices json file")

	makeDeviceSubfolders()

	return true
}

func loadAPIKeys() bool {
	f, err := os.Open(filepath.Join(saveDir, apiKeysFilename))
	if os.IsNotExist(err) {
		fmt.Println("API keys file does not exist. Creating...")
		return saveAPIKeys()
	} else if err != nil {
		fmt.Println("Error opening API keys file", err)
		return false
	}
	defer f.Close()

	decoder := json.NewDecoder(f)
	err = decoder.Decode(&apiKeys)
	if err != nil {
		fmt.Println("Error parsing API keys json file", err)
		return false
	}

	fmt.Println("Successfully loaded API keys json file")

	return true
}

func makeDeviceSubfolders() {
	for _, dev := range devices {
		err := os.Mkdir(filepath.Join(saveDir, "public", dev.DevID), 0700)
		if err != nil && !os.IsExist(err) {
			log.Fatal(err)
		}
	}
}

func saveDevices() bool {
	f, err := os.OpenFile(filepath.Join(saveDir, devicesFilename), os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0600)
	if err != nil {
		fmt.Println("Error opening devices file", err)
		return false
	}
	defer f.Close()

	encoder := json.NewEncoder(f)
	err = encoder.Encode(devices)
	if err != nil {
		fmt.Println("Error encoding devices json file", err)
		return false
	}

	fmt.Println("Successfully saved devices json file")
	return true
}

func saveAPIKeys() bool {
	f, err := os.OpenFile(filepath.Join(saveDir, apiKeysFilename), os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0600)
	if err != nil {
		fmt.Println("Error opening API keys file", err)
		return false
	}
	defer f.Close()

	encoder := json.NewEncoder(f)
	err = encoder.Encode(apiKeys)
	if err != nil {
		fmt.Println("Error encoding API keys json file", err)
		return false
	}

	fmt.Println("Successfully saved API keys json file")
	return true
}

func checkAPIKey(reading deviceJSON) bool {
	apiKey := apiKeys[reading.BoardID]

	// in case an empty apikey field sent and the API key doesn't exist in the store
	if apiKey == "" {
		return false
	}

	if apiKey != reading.APIKey {
		return false
	}

	return true
}
