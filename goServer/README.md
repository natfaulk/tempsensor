# Temperature sensor server

This is my first proper Go server application and is therefore is a bit of a mess I think....  

## Production install
The server requires the frontend to be built into the public/build directory
See the readme in the frontend directory. The front end is built using react.    

To build the backend, run `go build` in this directory  

To configure the port on which the server runs, copy/rename the example_config.yaml file in the config directory to config.yaml and change the port. I am running it behind NGINX which is being used as a reverse proxy.  

A systemctl file (tempsensor.service) is provided and needs to be copied to `/etc/systemd/system/`  
To start: `systemctl start tempsensor.service`  
To restart: `sudo systemctl daemon-reload`  
To view output: `sudo journalctl -f -u tempsensor.service`  

## Development install
The front end can be run in development mode (`npm start`) and requests are proxied through to the backend. The proxy address can be modified in the package.json file in the frontend root directory - ie. to point the proxy to the port the backend is running on.   

To build the backend, run `go build` in this directory and then run the executable directly  

## Current implementation and future development
Currently there are no caching headers being sent. This needs to be implemented so that cloudflare (I run my server behind cloudflare) caches things and reduces the server load.  

Currently data is saved on disk as JSON. Each reading is appended to a file and a new file made each day per device. For the small number of devices/site visitors this is fine but this is not scalable. The next step is to implement a database (probably mongoDB). After that some database caching such as Redis or Memcached would be a good idea.  

Currently there is no provisioning for new devices. Data sent from devices which are not in the devices.json file is ignored. Currently one has to manually edit this file to add a new device. A provisioning page needs to be added to initialise devices, as well as a page to manage devices.  

There is no authentication. Need authenticaiton on device provisioning and management pages when implemented. Also devices should send an API key to authenticate with the server to stop unauthorised people posting data.  

The code is not very well organised, and could be made into smaller subpackages to tidy things up.  

