module bitbucket.org/natfaulk/tempsensor

go 1.14

require (
	github.com/julienschmidt/httprouter v1.3.0
	gopkg.in/yaml.v2 v2.2.8
)
