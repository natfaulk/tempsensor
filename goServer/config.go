package main

import (
	"log"
	"os"
	"path/filepath"

	"gopkg.in/yaml.v2"
)

const (
	configDir  = "config"
	configFile = "config.yaml"
)

type Config struct {
	Server struct {
		Port string `yaml:"port"`
	} `yaml:"server"`

	Security struct {
		RequireAPIKeys bool `yaml:"requireAPIKeys"`
	} `yaml:"security"`
}

func makeDefaultConfig() Config {
	var cfg Config
	cfg.Server.Port = "3005"
	cfg.Security.RequireAPIKeys = true

	return cfg
}

func loadConfig() Config {
	cfg := makeDefaultConfig()

	configPath := filepath.Join(configDir, configFile)

	if _, err := os.Stat(configPath); os.IsNotExist(err) {
		log.Printf("No config file found, using defaults")
		return cfg
	}

	f, err := os.Open(configPath)
	if err != nil {
		log.Printf("Error opening config file. %v\n", err)
		log.Println("Using default config")
		return cfg
	}
	defer f.Close()

	decoder := yaml.NewDecoder(f)
	err = decoder.Decode(&cfg)
	if err != nil {
		log.Printf("Error parsing config file. %v\n", err)
		log.Println("Using default config")
	}

	return cfg
}
