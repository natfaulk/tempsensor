package main

import (
	"log"
	"os"
	"path/filepath"
)

// TODO: make this better
var gloablCfg Config

func main() {
	// make folder to hold the files, ignore the error if it already exists
	err := os.Mkdir(saveDir, 0700)
	if err != nil && !os.IsExist(err) {
		log.Fatal(err)
	}

	err = os.Mkdir(filepath.Join(saveDir, "public"), 0700)
	if err != nil && !os.IsExist(err) {
		log.Fatal(err)
	}

	loadDevices()
	loadAPIKeys()

	gloablCfg = loadConfig()
	beginServer(gloablCfg)
}
