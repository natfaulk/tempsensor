package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"path/filepath"
	"strings"

	"github.com/julienschmidt/httprouter"
)

func beginServer(cfg Config) {
	router := httprouter.New()
	router.GET("/", rootHandler)
	router.GET("/alldevices", devicesHandler)
	router.GET("/datafilenames/:devid", dataFilenamesHandler)
	router.POST("/add_data", addDataHandler)

	router.ServeFiles("/datafiles/*filepath", http.Dir(filepath.Join(saveDir, "public")))
	router.ServeFiles("/public/*filepath", http.Dir(filepath.Join("public", "build")))

	fmt.Printf("Starting server on port: %s\n", cfg.Server.Port)
	http.ListenAndServe(fmt.Sprintf(":%s", cfg.Server.Port), router)
}

func rootHandler(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
	if req.URL.Path != "/" {
		http.NotFound(w, req)
		fmt.Println("this ran")
		return
	}

	// res.Header().Set("Cache-Control", "public, max-age=86400")
	http.ServeFile(w, req, filepath.Join("public", "build", "index.html"))
}

func devicesHandler(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	if len(devices) > 0 {
		json.NewEncoder(w).Encode(devices)
	} else {
		fmt.Fprint(w, "[]")
	}
}

func dataFilenamesHandler(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
	filenames := getFilenames(ps.ByName("devid"))

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	if len(filenames) > 0 {
		json.NewEncoder(w).Encode(filenames)
	} else {
		fmt.Fprint(w, "[]")
	}
}

func addDataHandler(w http.ResponseWriter, req *http.Request, ps httprouter.Params) {
	if req.Method != http.MethodPost {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusMethodNotAllowed)
		fmt.Fprintf(w, "{\"success\":false,\"error\":\"Needs to be a post request\"}")
		return
	}

	if req.Header.Get("Content-Type") != "application/json" {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnsupportedMediaType)
		fmt.Fprintf(w, "{\"success\":false,\"error\":\"Needs to be json\"}")
		return
	}

	decoder := json.NewDecoder(req.Body)
	var json deviceJSON
	err := decoder.Decode(&json)

	if err != nil {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusBadRequest)
		fmt.Fprintf(w, "{\"success\":false,\"error\":\"Json failed to decode\"}")
		fmt.Println(err)
		return
	}

	if gloablCfg.Security.RequireAPIKeys && !checkAPIKey(json) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusUnauthorized)
		fmt.Fprintf(w, "{\"success\":false,\"error\":\"API key error\"}")
		fmt.Println("API key authorisation failed")
		return
	}

	if !storeReading(json) {
		w.Header().Set("Content-Type", "application/json")
		w.WriteHeader(http.StatusInternalServerError)
		fmt.Println("Failed to store reading (is the device ID in devices.json?)")
		fmt.Fprintf(w, "{\"success\":false,\"error\":\"Failed to save data\"}")
		return
	}

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	fmt.Fprintf(w, "{\"success\":true}")
}

func getFilenames(devid string) []string {
	files, err := ioutil.ReadDir(filepath.Join(saveDir, "public", devid))
	if err != nil {
		log.Println(err)
		return []string{}
	}

	var filenames []string
	for _, file := range files {
		if !file.IsDir() && strings.HasPrefix(file.Name(), saveFileStub) {
			filenames = append(filenames, file.Name())
		}
	}

	return filenames
}
