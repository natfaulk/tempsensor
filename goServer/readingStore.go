package main

import (
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"time"
)

type deviceJSON struct {
	Temp       float32 `json:"temp"`
	BatVoltage float32 `json:"batV"`
	FwVersion  string  `json:"VERSION"`
	BoardID    string  `json:"ID"`
	APIKey     string  `json:"apiKey"`
}

type storedJSON struct {
	Temp       float32 `json:"temp"`
	BatVoltage float32 `json:"batV"`
	Time       int64   `json:"time"`
	ID         string
}

const (
	settingsFile string = "settings.json"
	saveFileStub string = "data"
	saveDir      string = "data"
)

func storeReading(reading deviceJSON) bool {
	var readingOut = storedJSON{reading.Temp, reading.BatVoltage, time.Now().Unix() * 1000, reading.BoardID}

	// TODO: convert devices to a map
	devFound := false
	for i := range devices {
		if devices[i].DevID == readingOut.ID {
			devices[i].LastSeen = readingOut.Time
			devices[i].BatVoltage = readingOut.BatVoltage
			devFound = true
			break
		}
	}

	if !devFound {
		return false
	}

	saveDevices()

	savePath := filepath.Join(saveDir, "public", readingOut.ID, getTodayFilename())
	f, err := os.OpenFile(savePath, os.O_CREATE|os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		return false
	}

	defer f.Close()

	encoder := json.NewEncoder(f)
	encoder.Encode(readingOut)

	return true
}

func getTodayFilename() string {
	t := time.Now().UTC()
	return fmt.Sprintf("%s_%s.log", saveFileStub, t.Format("2006-01-02"))
}
